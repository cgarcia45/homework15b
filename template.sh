# template.sh - edit this file
function usage
{
  echo "usage: $0 arguments ..."
  if [ $# -eq 1 ]
  then echo "ERROR: $1"
  fi
}

# Your script starts after this line.
name="Santiago Garcia"

if [ $# -eq 0 ];
then
	usage
else
	
	echo $name
	date
	echo

	for filename
	do
		case "$filename" in 
			TestError)
				usage "TestError found"
				echo "*****"
				;;
			now)
				date=$(date +%r)
				echo "It is now $date"
				echo "*****"
				;;
			*)
				usage "Do not know what to do with $filename"
				echo "*****"
				;;
		esac	
	done
fi
